const {getAdminDB} = require('./dbconnection')
const adminDbSchema= require('../models/admindbModel')

module.exports={
    getTenantModel:async()=>{
            let db =await getAdminDB();
            let model= db.model('tenants',adminDbSchema.TenantSchema);
            return model;
       },
    getOrganisationModel:async()=>{
        let db =await getAdminDB();
        let model= db.model('organisations',adminDbSchema.TenantOrganisationSchema);
        return model;
    },

    getTenantdbModel:async()=>{
        let db =await getAdminDB();
        let model= db.model('tenantdbs',adminDbSchema.DbSchema);
        return model;
    }

}