const mongoose = require('mongoose');
const {mongoOptions} =require('./option')
require('dotenv').config()
const jwt=require('../src/services/jwt');
let db
const connect = async (url) => {
    return new Promise(async (resolve, reject) => {
        const connection = await mongoose.createConnection(url, mongoOptions).asPromise();
        resolve(connection)
    })
};

const getTenantDB =async(req,callback)=>{
    let token= req.headers['x-access-tenant'];

    jwt.verifyTenantToken(token,async(err,response)=>{
        const dbName= response.id+ response.name
        if(err){
         return err
        }else{
        const uri = process.env.BASEDBACCESS + dbName + process.env.SUFFIXDBACCESS;
         db= db ? db:await connect(uri);
         let tenantDb = db.useDb(dbName, { useCache: true });
         return callback(err,tenantDb,uri)
        } 
       
    })
}

const getAdminDB=async()=>{
     const dbName= process.env.DBNAME
     let uri= process.env.ADMINDBACCESS;
     db=db? db:await connect(uri) ;
     let adminDb = db.useDb(dbName, { useCache: true });
    return adminDb
}

module.exports = {
    getTenantDB,
    getAdminDB
}