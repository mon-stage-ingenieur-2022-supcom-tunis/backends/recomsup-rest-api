
const {  ServerApiVersion } = require('mongodb');

module.exports = {

  
  mongoOption: {
      
       useNewUrlParser: true, 
       serverApi: ServerApiVersion.v1,
       socketTimeoutMS: 50000,
      useUnifiedTopology: true,
      useFindAndModify: false,
       useCreateIndex: false,
       poolSize: 10,
       bufferMaxEntries: 0,
      
}
};
