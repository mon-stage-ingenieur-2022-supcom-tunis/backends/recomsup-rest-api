const {getTenantDB} = require("./dbconnection");
const TenantDbSchema =require('../models/tenantdbModel')

module.exports={
     getCustomerModel : async (req,callback) => {
         await getTenantDB(req,async(err,db,uri)=>{
            let response =await db.model("customers", TenantDbSchema.CustomerSchema);
            return callback(err,response,uri)
        });
    },
    getItemModel:async(req,callback)=>{
        await getTenantDB(req,async(err,db,uri)=>{
            let response= await db.model("items", TenantDbSchema.ItemsSchema)
            return callback(err,response,uri)
        });
    },
    getRatingModel:async(req,callback)=>{
        await getTenantDB(req,async(err,db,uri)=>{
            let response= await db.model("ratings", TenantDbSchema.RatingSchema);
            return callback(err,response,uri)

        });
    },
    getRecomModel:async(req)=>{
        await getTenantDB(req,async(err,db,uri)=>{
        let response= await db.model("recommendations", TenantDbSchema.recommendationSchema);
        return callback(err,response,uri)

        });
    },
    

}