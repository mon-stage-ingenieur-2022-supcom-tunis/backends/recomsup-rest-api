const jwt = require('../src/services/jwt')

module.exports={
    authorized:(req,res,next)=>{
        const token = req.headers["x-access-token"];
        if(!token){
          return  res.status(403).json({error:'forbiden',message:'no token was provided'})
        }else{
            jwt.verifyToken(token,(err,response)=>{
                if(err){
                  return  res.status(err.code|500).json({error:err})
                }else{
                 return next()
                }
            })
        }
       
    },
    authorizedTenant:(req,res,next)=>{
        const token = req.headers["x-access-tenant"];
        if(!token){
          return  res.status(403).json({error:'forbiden',message:'no token was provided'})
        }else{
            jwt.verifyTenantToken(token,(err,response)=>{
                if(err){
                  return  res.status(err.code|500).json({error:err})
                }else{
                 return next()
                }
            })
        }
    }
}