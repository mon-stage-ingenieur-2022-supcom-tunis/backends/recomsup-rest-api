const mongoose = require('mongoose');
module.exports={
    DbSchema: new mongoose.Schema({
        name:{
            required:true,
            type:String,
            dropDups:true 
        },
        organisationId:{
            type:mongoose.Schema.Types.ObjectId,
            ref:'organisations',
            required:false
        },
        created_at:{
            type:Date,
            required:false,
            default: Date.now
        },
        updated_at:{
         type:Date,
         required:false,
         default: Date.now
     },
     apiKey:{
         type:String,
         required:true
     },
     production:{
         type:Boolean,
         required:false,
     }
       
    }),

    TenantSchema:new mongoose.Schema({
  
        first_name: {
            required: true,
            type: String
        },
        last_name: {
            required: true,
            type: String
        },
        user_name: {
            required: true,
            type: String
        },
        email: {
            required: true,
            type: String
        },
       
        password: {
            required: true,
            type: String
        },
        created_at:{
            type:Date,
            required:false,
            default: Date.now
        },
        updated_at:{
         type:Date,
         required:false,
         default: Date.now
     },
    
    
    }),
    TenantOrganisationSchema:new mongoose.Schema({

        name:{
            type:String
        },
        website:{
            type:String
        },
        tenantId: {
            required: true,
            type: mongoose.Schema.Types.ObjectId,
            ref:'tenants'
        },
        created_at:{
            type:Date,
            required:false,
            default: Date.now
        },
        updated_at:{
         type:Date,
         required:false,
         default: Date.now
     },
     
    })
}




