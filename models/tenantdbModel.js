const mongoose = require('mongoose');


module.exports={
    CustomerSchema:new mongoose.Schema({
        created_at:{
            type:Date,
            required:false,
            default: Date.now
        },
        updated_at:{
         type:Date,
         required:false,
         default: Date.now
        },
        customerId:{
            type:String,
            required:true,
            dropDups:true,
            unique: true

    
        }
     
    }),
    ItemsSchema:new mongoose.Schema({
        itemId:{
           type:String,
           required:true,
           dropDups:true,
           unique: true
    
        },
        created_at:{
            type:Date,
            required:false,
            default: Date.now
        },
        updated_at:{
         type:Date,
         required:false,
         default: Date.now
     },
     itemUrl:{
         type:[String],
         required:false
     }
       
    }),
   RatingSchema: new mongoose.Schema({

        itemId:{
            type:mongoose.Schema.Types.ObjectId,
            required:true,
            ref:'items',
        },
        customerId:{ 
         type:mongoose.Schema.Types.ObjectId,
         required:true,
         ref:'customers',
        },
        purchased:{
            type:Boolean,
            required:false,
            default:false
        },
        detailViews :{
           required:false,
           type:Boolean,
           default:false
        },
        created_at:{
            type:Date,
            required:false,
            default: Date.now
        },
        updated_at:{
         type:Date,
         required:false,
         default: Date.now
     },
     timestamp:{
         type:String,
         required:false
     },
     rating:{
         type:Number,
         required:true
     }
     
     }),
     recommendationSchema: new mongoose.Schema({
  
        itemId:{
            type:mongoose.Schema.Types.ObjectId,
            required:true,
            ref:'items',
            
        },
        customerId:{
         type:mongoose.Schema.Types.ObjectId,
         required:true,
         ref:'customers',
        },
     
        created_at:{
            type:Date,
            required:false,
            default: Date.now
        },
        updated_at:{
         type:Date,
         required:false,
         default: Date.now
     },
     score:{
         type:Number,
         required:false
     }
     
     })
    
}