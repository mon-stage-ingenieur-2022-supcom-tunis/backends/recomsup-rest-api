const adminModel = require('../../confs/admindb');
const Service=require('../services/utils');
const { validationResult } = require('express-validator');

module.exports={

    createTenant:async(req,res)=>{
        let tenantModel= await adminModel.getTenantModel();
        Service.jwt.hashPassword(req.body.password,(hash)=>{
             const tenant= new tenantModel({
                first_name:req.body.firstname,
                last_name:req.body.lastname,
                user_name:req.body.username,
                email:req.body.email,
                password:hash,
           })
           Service.create(tenant,async(err,response)=>{
            const errors = validationResult(req);

               if(err){
                   res.status(err.code||500).json({
                       success:false,
                       error:err.error||errors.array(),
                       code:err.code||500
                   })
               }else{
                   let organisationModel =await adminModel.getOrganisationModel();
                   let organisation = new organisationModel({
                    name:req.body.organisation,
                    tenantId:response._id,
                    website:req.body.web
                   })

                   Service.create(organisation,async(err,organisationresponse)=>{
                        if(err){
                
                            res.status(err.code||500).json({success:false,error:err.error,code:err.code||500})
                        }else{
                            await Object.assign(response,{organisation:organisationresponse});
                            let dbModel = await adminModel.getTenantdbModel();

                            let payload= {issuer:"recomSup.tn",name:req.body.name,id:organisationresponse._id,expiresIn:'30d'}
                            Service.jwt.signTenantToken(payload,async(err,token)=>{
                                if(err){
                                    res.status(err.code||500).json({error:err.error,success:false,code:err.code||500})
                                }else{
                                    let db = new dbModel({
                                        name:req.body.name,
                                        organisationId:organisationresponse._id,
                                        apiKey:token
                                    })
                                    Service.create(db,(err,dbresp)=>{
                                         if(err){
                                             res.status(err.code||500).json({success:false,error:err.error,code:err.code||500})
                                         }else{
                                            res.status(201).json({success:true,response:{tenant:response,organisation:organisationresponse,db:dbresp},code:201})

                                         }
                                    })
                                }
                            })
                            
                            


                        }
                   })
                   
               }
           })
        })
      
    },
    createOrganisation:async(req,res)=>{
           let organisationModel= await adminModel.getOrganisationModel();
           const tenant= req.body.tenant;
           const organisation= new organisationModel({
               name:req.body.organisation,
               tenantId:tenant,
               website:req.body.web
           });
           Service.create(organisation,(err,resp)=>{
               if(err){
                   res.status(err.code||500).json({error:err.error,success:false,code:err.code||500})
               }else{
                res.status(201).json({response:resp,success:true,code:201})
               }
           })
    },
    createdb:async(req,res)=>{
        let dbModel= await adminModel.getTenantdbModel();
        let organisation = req.body.organisation
         let payload= {issuer:"recomSup.tn",name:req.body.name,id:organisation,expiresIn:'30d'}
         Service.jwt.signTenantToken(payload,(err,token)=>{
          if(err){
              res.status(err.code||500).json({error:err.error,success:false})
           }else{
            let db = new dbModel({
                name:req.body.name,
                organisationId:organisation,
                apiKey:token
            })
            Service.create(db,(err,resp)=>{
                if(err){
                    res.status(err.code||500).json({error:err.error,code:err.code,success:false})
                }else{
                    res.status(201).json({success:true,response:resp,code:201})
                }
            })
          }
        })
      
    },

    getTenants:async(req,res)=>{
      let Tenant= await adminModel.getTenantModel();
     if(req.query.filter){
        Service.query.find(Tenant,req.query.filter,(err,response)=>{
            if(err){
                res.status(err.code||500).json({error:err.errort,code:err.code||500,success:false})
            }else{
                res.status(200).json({response:response,success:true,code:200})
            }
        })
     }else{
        Service.query.find(Tenant,null,(err,response)=>{
            if(err){
                res.status(err.code||500).json({error:err.errort,code:err.code||500,success:false})
            }else{
                res.status(200).json({response:response,success:true,code:200})
            }
        })
     }
    },

    signIn:async(req,res)=>{
     const errors = validationResult(req);
     if(!errors){
         res.status(400).json({
             error:errors.array(),
             success:false,
             code:400
         })
     }else{
         let tenantModel = await adminModel.getTenantModel()
         Service.auth.login(tenantModel,req.body,(err,response)=>{
            if(err){
            res.status(err.code||500).json({success:false,error:err.error})
            }else{
                let payload={
                    issuer:'recomSup.tn',
                    tenant:response._id,
                    email:response.email
                }
                Service.jwt.signToken(payload,async(err,token)=>{
                    if(err){
                        res.status(err.code||500).json({success:false,code:err.code||500,error:err.error});
                    }else{
                        response={
                            user_id:response._id,
                            email:response.email,
                            firstname:response.first_name,
                            lastname:response.last_name,
                            username:response.user_name
                        }

                        let organisation=await adminModel.getOrganisationModel()
                        Service.query.find(organisation,{tenantId:response.user_id},async(err,orgonisationResponse)=>{
                            if(err){
                                res.status(err.code||500).json({success:false,code:err.code||500,error:err.error})
                            }else{
                                let db =await adminModel.getOrganisationModel();
                                Service.query.find(db,{organisationId:orgonisationResponse._id},(err,dbResponse)=>{
                                    if(err){
                                        res.status(err.code||500).json({success:false,error:err.error,code:err.code||500})
                                    }else{
                                        res.status(200).json({success:true,code:200,response:{tenant:response,organisation:orgonisationResponse,db:dbResponse},token:token});

                                    }
                                })




                            }
                        })
                    }

                })
            }
         })
     }

    },
    resetpassword:async(req,res)=>{
       Service.jwt.hashPassword(req.body.password,async(hash)=>{
            let tenantdbModel = await adminModel.getTenantModel();
            let query = {email:req.body.mail};
            let update={password:hash}
            Service.update.findOneAndUpdate(tenantdbModel,query,update,(err,response)=>{
                if(err){
                   
                    res.status(err.code||500).json({success:false,error:err.error,code:err.code||500})
                }else{
                    res.status(200).json({success:true,response:response,code:200})
                }
            })
          
       })
    },
    getTenantInfo:async(req,res)=>{
    let organisation= await adminModel.getOrganisationModel()
    Service.query.find(organisation,{tenantId:req.query.user},async(err,response)=>{
    if(err){
        res.status(err.code|500).json({success:false,error:err.error,code:err.code||500})
    }else{
        let db = await adminModel.getTenantdbModel();
        Service.query.find(db,{organisationId:response[0]._id},(err,dbResponse)=>{
            if(err){
                res.status(err.code||500).json({success:false,error:err.error,code:err.code||500})
            }else{
                res.status(200).json({success:true,response:{organisation:response,db:dbResponse,success:true}})
            }
        })
    }
    })
    }


}

