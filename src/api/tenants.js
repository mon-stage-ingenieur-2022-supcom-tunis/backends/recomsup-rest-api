const Service=require('../services/utils');
const tenant = require('../../confs/tenantdb');
const ratings =require('../services/helpers/ratings')

module.exports={
      createItem:async(req,res)=>{
                 tenant.getItemModel(req,(err,model,uri)=>{
           if(err){
               res.status(err.code||500).json({error:err.error,success:false,code:err.code||500})
           }else{
            let item = new model({
                itemId:req.body.itemId,
                itemUrl:req.body.url
              });
              Service.create(item,(err,resp)=>{
                  if(err){
                      res.status(err.code||500).json({error:err.error,code:err.code,success:false})
                  }else{
                      res.status(201).json({success:true,response:resp,code:201})
                  }
              })
           }
          });
        
      },
      createCustomer:async(req,res)=>{
          tenant.getCustomerModel(req,(err,model,uri)=>{
              if(err){
                  res.status(err.code||500).json({
                      success:false,
                      error:err.error,
                      code:err.code||500
                  })
              }else{
                let customer = new model({
                    customerId:req.body.customer,
                });
                Service.create(customer,(err,response)=>{
                 if(err){
                    res.status(err.code||500).json({
                        success:false,
                        error:err.error,
                        code:err.code||500
                    })
                 }else{
                    res.status(201).json({
                        success:true,
                        code:201,
                        response:response
                    })
                 }
                })
               
              }
          })
      },
      createDetailView:async(req,res)=>{
        tenant.getRatingModel(req,async(err,model,uri)=>{
            if(err){
                res.status(err.status(err.code||500)).json({success:false,error:err.error,code:err.code||500})
            }else{
                  let data= await  model.find({customerId:req.body.customer,itemId:req.body.item});
                  if(data.length>0){
                      if(data[0].rating){
                       
                        ratings.findOneAndUpdate(model,{customerId:req.body.customer,itemId:req.body.item},{rating:data[0].rating+1},(err,response)=>{
                            if(err){
                                console.log(err)
                                res.status(err.code||500).json({code:err.code||500,success:false,error:err.error})
                            }else{
                              
                                Service.publish({data:response,token:req.headers["x-access-tenant"],user:req.body.customer,uri:uri},(err,r)=>{
                                   if(err){
                                    console.log(err)
                                    res.status(409).json({success:true,response:response,code:409})
                                   }else{
                                    res.status(201).json({success:true,response:response,code:201})
                                   }
  
    
                                 },'training')
                            }
                        })
                      }
                 
                  }else{
                    let rating = new model({
                        detailViews:true,
                        timestamp:req.body.timestamp,
                        customerId:req.body.customer,
                        itemId:req.body.item,
                        rating:1
                    });
                    ratings.create(rating,(err,response)=>{
                        if(err){
                            console.log(err)
                            res.status(err.code||500).json({code:err.code||500,success:false,error:err.error})
                        }else{
                            res.status(201).json({success:true,response:response,code:201})
                    
                        }
                    })
                  }
                
               
            }
        })
      },
      createPurchase:async(req,res)=>{
        tenant.getRatingModel(req,async(err,model,uri)=>{
            if(err){
                res.status(err.status(err.code||500)).json({success:false,error:err.error,code:err.code||500})
            }else{
                  let data= await  model.find({customerId:req.body.customer,itemId:req.body.item});
                  if(data.length>0){
                      
                        ratings.findOneAndUpdate(model,{customerId:req.body.customer,itemId:req.body.item},{rating:data[0].rating+1},(err,response)=>{
                            if(err){
                                res.status(err.code||500).json({code:err.code||500,success:false,error:err.error})
                            }else{
                                Service.publish({data:response,token:req.headers["x-access-tenant"],user:req.body.customer,uri:uri},(err,r)=>{
                                    console.log(err)
                                    res.status(201).json({success:true,response:response,code:201})
        
                                 },'training')
                            }
                        })
                  }else{
                    let rating = new model({
                        purchased:true,
                        timestamp:req.body.timestamp,
                        customerId:req.body.customer,
                        itemId:req.body.item,
                        rating:1
                    });
                    ratings.create(rating,(err,response)=>{
                        if(err){
                            res.status(err.code||500).json({code:err.code||500,success:false,error:err.error})
                        }else{
                            res.status(201).json({success:true,response:response,code:201})
                      
                        }
                    })
                  }
                
              
               
            }
        })
     },

     createRecommation:async(req,res)=>{
         tenant.getRecomModel(req,async(err,model,uri)=>{
             if(err){
                 res.status(err.code||500).json({success:false,error:err.error,code:err.code|500})
             }else{
                let Model = new model({
                    itemId:req.body.itemId,
                    customerId:req.body.customerId,
                    score:req.body.score
                })
                
                Service.create(Model,(err,response)=>{
                    if(err){
                        res.status(err.code||500).json({success:false,error:err.error,code:err.code||500})
                    }else{
                        res.status(201).json({success:true,response:response,code:201})
                    }
                })
             }
         })
     },


     getRecommendations:async(req,res)=>{
         tenant.getRecomModel(req,async(err,model,uri)=>{
             if(err){
                 res.status(err.code||500).json({success:false,error:err.error,code:err.code||500})
             }else{
                Service.query.find(model,{},(err,response)=>{
                    if(err){
                        res.status(err.code||500).json({success:false,error:err.error,code:err.code||500})
                    }else{
                        res.status(200).json({success:true,response:response,code:200})
                    }
                })

             }
         })
     },

     getRecommendation:async(req,res)=>{
        tenant.getRecomModel(req,(err,model,uri)=>{
            if(err){
         
                res.status(err.code||500).json({success:false,error:err.error,code:err.code||500})
            }else{

                tenant.getItemModel(req,(err,Itemmodel,uri)=>{
                    
               Service.query.find(model,{
                "$lookup": {
                    "from": "item",
                    "localField": "projects.tags",
                    "foreignField": "_id",
                    "as": "resultingTagsArray"
                }},req.params.limit,(err,response)=>{
                if(err){
                    res.status(err.code||500).json({success:false,error:err.error,code:err.code||500})
                }else{
                    res.status(200).json({success:true,response:response,code:200})
                }
            })

                })


            }
        })
    },

    getRatings(req,res){
     tenant.getRatingModel(req,async(err,model,uri)=>{

            if(err){
                res.status(err.code||500).json({success:false,error:err.error,code:err.code||500})
            }else{
               Service.query.find(model,null,(err,response)=>{
                   if(err){
                       res.status(err.code||500).json({success:false,error:err.error,code:err.code||500})
                   }else{
                       res.status(200).json({success:true,response:response,code:200})
                   }
               })

            }
        })
    },
    getItems(req,res){
        tenant.getItemModel(req,async(err,model,uri)=>{
   
               if(err){
                   res.status(err.code||500).json({success:false,error:err.error,code:err.code||500})
               }else{
                  Service.query.find(model,null,null,(err,response)=>{
                      if(err){
                          res.status(err.code||500).json({success:false,error:err.error,code:err.code||500})
                      }else{
                          res.status(200).json({success:true,response:response,code:200})
                      }
                  })
   
               }
           })
       },
       getCustomers(req,res){
        tenant.getCustomerModel(req,async(err,model,uri)=>{
   
               if(err){
                   res.status(err.code||500).json({success:false,error:err.error,code:err.code||500})
               }else{
                  Service.query.find(model,null,(err,response)=>{
                      if(err){
                          res.status(err.code||500).json({success:false,error:err.error,code:err.code||500})
                      }else{
                          res.status(200).json({success:true,response:response,code:200})
                      }
                  })
   
               }
           })
       }

}