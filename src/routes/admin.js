const adminApi= require('../api/admin')
const passport = require('../../middlewares/passport')
const express= require('express')
const route= express.Router();
const { body, validationResult } = require('express-validator');

 route.use('/info',passport.authorized)
route.use('/db',passport.authorized)
route.use('/organisations',passport.authorized);

 route.post('/', body('email').isEmail(), body('password').isLength({ min: 8 }),async(req,res)=>{
  adminApi.createTenant(req,res)
 });

 route.post('/organisations',async(req,res)=>{
     adminApi.createOrganisation(req,res)
 })

 route.post('/db',async(req,res)=>{
     adminApi.createdb(req,res)
 })
route.get('/',async(req,res)=>{
    adminApi.getTenants(req,res)
});

route.get('/info',(req,res)=>{
    adminApi.getTenantInfo(req,res)
});

route.post('/auth',body('email').isEmail(), body('password').isLength({ min: 8 }),(req,res)=>{
adminApi.signIn(req,res)
});

route.patch('/password',body('password').isLength({ min: 8 }),(req,res)=>{
    adminApi.resetpassword(req,res)
    })

 
 module.exports=route