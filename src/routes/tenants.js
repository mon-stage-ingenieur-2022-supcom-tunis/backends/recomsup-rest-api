const express= require('express');
const tenantApi= require('../api/tenants');
const passport = require('../../middlewares/passport');

const router = express.Router();

router.use('/',passport.authorizedTenant)

router.post('/items',async(req,res)=>{
    tenantApi.createItem(req,res)
});

router.post('/customers',async(req,res)=>{
    tenantApi.createCustomer(req,res)
});
router.post('/detail-views',async(req,res)=>{
    tenantApi.createDetailView(req,res)
});

router.post('/purchases',async(req,res)=>{
    tenantApi.createPurchase(req,res)
});

router.get('/recommendations',async(req,res)=>{
    tenantApi.getRecommendations(req,res)
});
router.get('/ratings',async(req,res)=>{
     tenantApi.getRatings(req,res)
});

router.get('/items',async(req,res)=>{
    tenantApi.getItems(req,res)
});
router.get('/customers',async(req,res)=>{
    tenantApi.getCustomers(req,res)
});


router.post('/recommendations',async(req,res)=>{
    if(!req.query){
        tenantApi.createRecommation(req,res)

    }else{
        tenantApi.createRecommation(req,res)

    }
});





module.exports=router

