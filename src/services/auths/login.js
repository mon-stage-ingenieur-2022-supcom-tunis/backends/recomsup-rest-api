const bcrypt = require('bcrypt');
module.exports= {
    login:async(model,user,callback)=>{
        try{
            let response =await model.findOne({email:user.email})
            if(response){
              await  bcrypt.compare(user.password, response.password, async function(err, result) {
                if(!result){
                    return callback({error:'password not found',code:401},response)

                }else{

                    return callback(null,response)

                }
                });
             
            }else{
                return callback({error:'Email not found',code:401},response)
            }
            
        }catch(err){
            return callback({error:err,code:500},null)

        }
    
    },

  
}

