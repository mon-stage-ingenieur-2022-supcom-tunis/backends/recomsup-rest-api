module.exports={
        find:async(model,param=null,limit=null,callback)=>{
            if(!param){
                try{
                    let response= await model.find({});
                    return callback(null,response)
                }catch(err){
                     return callback({error:err,code:500},null)
                }
               }else{
                try{
                  if(limit){
                    let response= await model.find(param).limit(limit)
                    return callback(null,response)

                  }{
      
                let response= await model.find(param)
                return callback(null,response)

                  }
              
                }catch(err){
                    return callback({error:err,code:400},null)
                }
                }
              },

              findOne:async(model,query,callback)=>{
                try{
                  let response =await model.findOne(query);
                  return callback(null,response)
                }catch(err){
                  return callback(err,null)
                }
         },
 
      

}