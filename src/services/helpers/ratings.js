const query = require("./query")


module.exports={ 
    
    create: async(model,callback)=>{
     try{
       
        let response=    await  model.save()
  
         return callback(null,response)

           }
           catch(err){
    
           return callback({error:err,code:400},null)
            }
        },

       findOneAndUpdate:async(model,conditions,update,callback)=>{
     
             model.findOneAndUpdate(conditions,update,async function(error,result){
                 if(error){
                   return callback({error:error,code:400},null)
                 }else{
                 
                     return await query.find(model,null,null,callback)
                 }
               });
             
         
         }
     


 
 }