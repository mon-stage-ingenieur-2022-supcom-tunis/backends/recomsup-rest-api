module.exports={
    
       findOneAndUpdate:async(model,conditions,update,callback)=>{

        model.findOneAndUpdate(conditions,update,function(error,result){
            if(error){
              return callback({error:error,code:400},null)
            }else{
                return callback(null,result)
            }
          });
        
    
    }
}