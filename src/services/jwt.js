const fs = require('fs')
const path = require('path')
const jwt = require('jsonwebtoken')
const privateKey = fs.readFileSync(path.join('src/keys', 'rsa.key'), 'utf8')
const publicKey = fs.readFileSync(path.join('src/keys', 'rsa.pub.key'), 'utf8')
const apiPublicKey=fs.readFileSync(path.join('src/keys','api.rsa.pub.key'),'utf8')
const apiPrivateKey = fs.readFileSync(path.join('src/keys','api.rsa.private.key'),'utf8')
const bcrypt = require('bcrypt');
const saltRounds = 10;

module.exports = {

    signToken: async(payload,callback) => {
        try {
            let token = await jwt.sign(payload, privateKey, { algorithm: 'RS256'})
            return callback(null,token);
        } catch (err) {
            console.log(err)
            return callback({error:err,code:400},null)
        }
    },

    verifyToken: async(token,callback) => {
        try {
           let verified=await jwt.verify(token, publicKey, { algorithm: 'RS256'})
            return callback(null,verified);
        } catch (err) {
          return callback({error:err,code:401},null)
        }
    },


    signTenantToken:async(payload,callback)=>{
        try {
            let token= await jwt.sign(payload, apiPrivateKey, { algorithm: 'RS256'});
            return callback(null,token)
        } catch (err) {
            return callback({error:err,code:400},null)
        }
    },

    verifyTenantToken: async(token,callback)=>{
        try {
            let verified=await jwt.verify(token, apiPublicKey, { algorithm: 'RS256'})
             return callback(null,verified);
         } catch (err) {
           return callback({error:err,code:401},null)
         }
    },
  
    hashPassword:async(password,callback)=>{
      await  bcrypt.hash(password, saltRounds, function(err, hash) {
            return callback(hash)
        });

    }


}
