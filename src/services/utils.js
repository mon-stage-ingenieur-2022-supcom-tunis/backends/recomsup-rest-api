const {create} =require('./helpers/create');
const query=require('./helpers/query');

const jwt=require('./jwt');
const auth= require('./auths/login');
const update=require('./helpers/update');
const { Publisher } = require('./helpers/producer');
module.exports={
    query:query,
    create:create,
    jwt:jwt,
    auth:auth,
    update:update,
    publish:Publisher,

}